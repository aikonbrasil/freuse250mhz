# Dynamic Algorithm for Interference Mitigation Between Cells in Networks Operating in the 250 MHz Band

An open source simulator is calibrated with data obtained by a drive test on a real scenario of the network operating in 250~MHz in Brazil. Thus, our results provide a strong representation of what is expected in real scenarios.

## ABSTRACT

The growing demand for Internet of Things (IoT) applications in agribusiness increases the necessity of reliable and secure connectivity in rural areas. Thus, in the particular case of Brazil, some initiatives aim to take advantage of frequency bands dedicated to limited private services. For instance, cellular networks based on orthogonal frequency-division multiple access (OFDMA) in 250 MHz bands require specialized adaptations because the interference between cells increases when these systems operate in the VHF band.
This work presents an analysis based on a reliable simulation of interference mitigation in OFDMA systems at 250 MHz using a network simulator. The value of this simulation is in that it was
The simulator is calibrated with data obtained in the field by an extensive and rigorous drive test. Therefore, the analysis is based on a comparison of traditional frequency reuse schemes with a machine learning approach based on deep reinforcement learning (DRL) to reduce inter-cell interference. The numerical results indicate that the DRL approach outperforms the traditional FR schemes in four different typical agribusiness scenarios.
